#include <stdio.h>
#include<iostream>
using namespace std;
void update(int* a, int* b) {
    int sum, subs;
    sum = *a + *b;
    subs = *a - *b;
    if (subs < 0)
        subs = -1 * subs;
    *a = sum;
    *b = subs;
}
   /**
   * gets two input from user to add and subtract via pointer
   */
int main() {
    int a, b;
    int* pa = &a, * pb = &b;
    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d\n", a, b);

    system("pause");
}