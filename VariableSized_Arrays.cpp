#include <stdio.h>
#include<stdlib.h>
#include <vector>
#include <iostream>
using namespace std;
 /**
 *I wanted to get the index and print it, it gave runtime error,
 *and I found a solution as below.
 */
int main() {
        int n, q, k, element, i = 0, index1, index2;
        cin >> n;
        cin >> q;
        vector<vector<int>> variableArrays;
        for (int j = 0; j < n; j++) {
            k = 0;
            cin >> k;
            vector<int>Array;
            for (i = 0; i < k; i++) {
                cin >> element;
                Array.push_back(element);
            }
            variableArrays.push_back(Array);
        }
        q = q * q;
        int* indexarray = new int[q];
        for (i = 0; i < q; i++) {
            cin >> index1;
            indexarray[i]=index1;
         }
        for (i = 0; i < q;i=i+2) {
            index1 = indexarray[i];
            index2 = indexarray[i + 1];
            cout << variableArrays[index1][index2] << endl;
        }
    system("pause");
}