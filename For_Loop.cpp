#include <iostream>
#include <cstdio>
using namespace std;
    /**
    *get 2 integer and the numbers that between them
    *if smaller than 9 print that integer
    *else print odd or even
    */
int main() {
    int a, b, n;
    cin >> a;
    cin >> b;
    for (n = a; n <= b; n++) {
        if (n >= 1 && n <= 9) {
            if (n == 9)
                printf("nine");
            if (n == 8)
                printf("eight");
            if (n == 7)
                printf("seven");
            if (n == 6)
                printf("six");
            if (n == 5)
                printf("five");
            if (n == 4)
                printf("four");
            if (n == 3)
                printf("three");
            if (n == 2)
                printf("two");
            if (n == 1)
                printf("one");
            cout << endl;
        }
        else {
            if (n % 2 == 0)
                printf("even");
            else
                printf("odd");
            cout << endl;
        }
    }
   system("pause");
}