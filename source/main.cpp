#include<iostream>
#include<fstream>
int index,i=0,sum=0,multiply=1,smallest,j;
float average;
using namespace std;
void sumfunction(int array[]) {
	for (j = 0; j < i; j++)
		sum = sum + array[j];
}
void multiplyfunction(int array[]) {
	for (j = 0; j < i; j++)
		multiply = multiply * array[j];
}
void smallestfunction(int array[]) {
	smallest = array[0];
	for (j = 0; j < i; j++) {
		if (array[j] < smallest)
			smallest = array[j];
	}
}
void averagefunction(int array[]) {
	average = (float)sum / i;
}
int main(){
	char filename[30];
	FILE* file;
	int number;
	int *arraynumbers;
	cout<<"Enter the name of the file you want to process : ";
	cin >> filename;
	fopen_s(&file,filename, "r");
	while (1) { //reading data from file
		if (!file) {
			cout<<"Wrong name or no such file exists! Enter again: "; 
			cin >> filename;
			fopen_s(&file, filename, "r");
		}
		else {
			cout << "�nput file : " << endl;
			fscanf_s(file, "%d", &index);
			cout << index<<endl;
			arraynumbers = new int[index];
				while (fscanf_s(file, "%d", &number) != EOF) {
					arraynumbers[i] = number;
					cout << number << "\t";
					i++;
				}
				if (i < index)
					cout << "The file does not have as many integers as the number specified, but can be processed!";
			break;
		}
	}
	cout << endl;
	sumfunction(arraynumbers);
	multiplyfunction(arraynumbers);
	smallestfunction(arraynumbers);
	averagefunction(arraynumbers);
	if (sum != 0 && multiply != 0 && i != 0) { //check if the file is empty
		cout << "+*******Results*******+" << endl;
		cout << "Sum is : " << sum << endl;
		cout << "Product is : " << multiply << endl;
		cout << "Average is : " << average << endl;
		cout << "Smallest is : " << smallest << endl;
	}
	else {
		cout << "No data in File!!" << endl;
	}
	delete[]arraynumbers;
	arraynumbers = nullptr;
	fclose(file);
	system("pause");
}
