#include <sstream>
#include <vector>
#include <iostream>
#include<string.h>
using namespace std;

vector<int> parseInts(string str) {
    int n = str.length();
    int num = 0, control = 0, negative = 1;
    vector<int> tointeger;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] != ',') {
            if (str[i] == '-') {
                negative = -1;
                i++;
            }
            if (control > 0) {
                num = num * 10;
            }
            num = num + negative * (str[i] - '0');
            control++;
        }
        else {
            negative = 1;
            tointeger.push_back(num);
            num = 0;
            control = 0;
        }

    }
    tointeger.push_back(num);
    return tointeger;
}
/// enter a string Containing numbers and a comma in between
int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    system("pause");
}
