﻿#include<iostream>
using namespace std;
   /**
    *Integer values ​​between 1 and 9 entered are written in English equivalent.
    *If greater than 9, "Greater than 9" is written.
    */
int main()
{
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    if (n > 9)
        printf("Greater than 9");
    if (n == 9)
        printf("nine");
    if (n == 8)
        printf("eight");
    if (n == 7)
        printf("seven");
    if (n == 6)
        printf("six");
    if (n == 5)
        printf("five");
    if (n == 4)
        printf("four");
    if (n == 3)
        printf("three");
    if (n == 2)
        printf("two");
    if (n == 1)
        printf("one");
    cout << endl;
    system("pause");
}
