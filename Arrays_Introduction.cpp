#include <iostream>
using namespace std;
/**
    *get array size and gets numbers in array
    *prints reverse order
    */
int main() {
    int* numbers, n;
    cin >> n;
    numbers = new int[n];
    for (int i = 0; i < n; i++) {
        cin >> numbers[i];
    }
    for (int i = n - 1; i >= 0; i--) {
        cout << numbers[i] << " ";
    }
    cout << endl;
    system("pause");
}
