#include <iostream>
#include<stdio.h>
using namespace std;
/// cout causes approximate error so I used printf
int main() {
    int a;
    long b;
    char c;
    float f;
    double d;
    cin >> a >> b >> c >> f >> d;
    printf("%d\n%ld\n%c\n%f\n%lf\n", a, b, c, f, d); 
    system("pause");
}